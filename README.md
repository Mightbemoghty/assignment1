# README #

This project is written in C++, and could be set up in Visual Studio

## What is this repository for? ##

* Quick summary: this project is designed to simulate the card game siete y medio with the user playing against the program;

* Version 1.01

## How do I get set up? ##
* There are one header file, two other .cpp files; one contains all the implementations of classes defined in the head file, and the other contain the main routine of the card game
* Three classes were created including player (to mimic dealer and participant), hand (referring to the cards one player holds every round), and card class (which mimics each single card one might draw);
* How to run tests: all card drawing is randomized within the program, user could test the program by inputting different bet amount, as long as it remain less than or equal to the total money player has.

## Contribution guidelines ##

* Contribution could be made through commiting a pull request and will be approved after test

### Who do I talk to? ###

* the repo owner could be reached at xdfcaiwensheng@gmail.com
* Other contributor: 1499370766@qq.com