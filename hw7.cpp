#include "cards.h"
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Global constants (if any)

// Non member functions declarations (if any)

// Non member functions implementations (if any)

// Stub for main
int main() {
  /* --STATEMENTS-- */
  Player participant(100);
  Player dealer(0);
  int round = 1;
  int bet = 0;
  ofstream game_file;
  game_file.open("gamelog.txt");
  while (participant.get_money() > 0 && dealer.get_money() > -900) {
	Hand participant_hand;
	Hand dealer_hand;
    // input validation for bet amount
    do {
      cout << "You have $" << participant.get_money() << ". Enter bet: ";
      cin >> bet;
    } while (bet > participant.get_money());
    
	// player's round
    string choice;
    do {
		Card new_card;
		participant_hand.add_card(new_card);
		if (choice == "y" && participant_hand.get_total() < 7.5) {
			cout << "New card:\n"
				<< " " << new_card.get_spanish_rank() << " de "
				<< new_card.get_spanish_suit() << " ("
				<< new_card.get_english_rank() << " of "
				<< new_card.get_english_suit() << ").\n\n";
		}
		cout << "Your cards:\n";
		participant_hand.print_hand();
		cout << "Your toal is " << participant_hand.get_total()
			<< ". Do you want another card (y/n)? ";
		cin >> choice;
	} while (choice == "y" && participant_hand.get_total() < 7.5);
    
	// dealer's round
    Card new_card;
    dealer_hand.add_card(new_card);
    while (dealer_hand.get_total() < 5.5) {
      cout << "Dealer's cards:\n";
      dealer_hand.print_hand();
      cout << "The dealer's total is " << dealer_hand.get_total() << ".\n\n";
      if (dealer_hand.get_total() < 5.5) {
        Card new_card;
        dealer_hand.add_card(new_card);
        cout << "New card:\n"
             << " " << new_card.get_spanish_rank() << " de "
             << new_card.get_spanish_suit() << " ("
             << new_card.get_english_rank() << " of "
             << new_card.get_english_suit() << ").\n\n";
		cout << "Dealer's cards:\n";
		dealer_hand.print_hand();
		cout << "The dealer's total is " << dealer_hand.get_total() << ".\n\n";
      }
    }
	//record game in file
	for (int i = 0; i < 60; i++) {
		game_file << "-";
	}
	game_file << "\n\nGame number: " << round << "    Money Left: $" << participant.get_money()
		<< "\nBet: " << bet << "\n\nYour total: " << participant_hand.get_total()
		<< "\n\nDealer's total is " << dealer_hand.get_total() << "\n\n";
	round++;

    // compare hands
    /*
    if (participant_hand.get_total() > 7.5) {
            cout << "Too bad. You lose " << bet << ".\n\n";
            participant.update_money(-bet);
            dealer.update_money(bet);
    }
    else if (participant_hand.get_total() > dealer_hand.get_total()) {
            cout << "You win " << bet << ".\n\n";
            participant.update_money(bet);
            dealer.update_money(-bet);
    }
    else if (participant_hand.get_total() < dealer_hand.get_total()) {
            cout << "Too bad. you lose " << bet << ".\n\n";
            participant.update_money(-bet);
            dealer.update_money(bet);
    }
    else {
            cout << "Nobody wins!\n\n";
    }
    */
    bool participant_win;
    if (participant_hand.get_total() > 7.5) {
      participant_win = false;
    } 
	else {
		if (participant_hand.get_total() > dealer_hand.get_total()) { participant_win = true; }
		else if (participant_hand.get_total() < dealer_hand.get_total() && dealer_hand.get_total() > 7.5) { participant_win = true; }
		else if (participant_hand.get_total() < dealer_hand.get_total() && dealer_hand.get_total() < 7.5) { participant_win = false; }
		else { cout << "Nobody wins.\n\n"; }
	}
    if (participant_win) {
      cout << "You win " << bet << ".\n\n";
      participant.update_money(bet);
      dealer.update_money(-bet);
    }
	else if (!participant_win) {
      cout << "Too bad. you lose " << bet << ".\n\n";
      participant.update_money(-bet);
      dealer.update_money(bet);
    }
	else { }
  }

  //end greeting
  if (participant.get_money() == 0) {
	  cout << "You have $0. GAME OVER!\n"
		  << "Come back when you have more money.\n\n";
  }
  else {
	  cout << "Dealer lost too much. GAME OVER!\n"
		  << "Thank you for playing.\n\n";
  }
  cout << "Bye!";
  return 0;
}
